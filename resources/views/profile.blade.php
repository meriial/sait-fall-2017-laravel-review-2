@extends('layouts.app')

@section('content')
    <h2>Edit Profile</h2>
    <img src="<?php echo asset('storage/'.$user->profile_image); ?>" alt="">
    <form action="/profile" method="post" enctype="multipart/form-data">
        <?php echo csrf_field() ?>
        <input type="file" name="image">

        <input type="submit" name="submit" value="Submit">
    </form>
@endsection
