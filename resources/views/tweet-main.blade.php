<?php if(Auth::check()) { ?>
    <form action="/tweet" method="post">
        <?php echo csrf_field() ?>
        <div class="<?php echo $errors->has('content') ? 'error' : '' ?>">
            <textarea name="content" rows="8" class="width-100"></textarea>
            <span><?php echo $errors->first('content') ?></span>
        </div>
        <input type="submit" name="submit" value="Tweet!">
    </form>
<?php } ?>

<ul class="list-unstyled margin-top-20">
    <?php foreach($tweetList as $tweet) { ?>
        <li>

            <div class="flex-container">
                <div class="tweet-image">
                    <img src="/storage/{{ $tweet->user->profile_image }}" alt="" class="img-responsive">
                </div>
                <div class="margin-left-40">
                    <a href="/user/<?php echo $tweet->user->id ?>"><?php echo $tweet->user->name ?></a>
                </div>
            </div>
            <?php echo $tweet->content ?>
        </li>
    <?php } ?>
</ul>
