@extends('layouts.app')

@section('content')

    <style media="screen">
        .main-header-img {
            background-image: url('<?php echo $headerImage ?>')
        }
    </style>

    <header class="margin-bottom-20 main-header-img">
    </header>

    <div class="flex-container">
        <div class="flex-1 padding-20">
            <?php if($user) { ?>
                <h2><?php echo $user->name ?></h2>
                <div class="">
                    <img src="/storage/<?php echo $user->profile_image ?>" alt="" class="img-responsive">
                </div>
            <?php } else { ?>
                <span>Not a member?
                    <a href="/register" class="btn btn-primary">Join Now!</a>
                </span>
            <?php } ?>
        </div>
        <div class="flex-2">
            @include('tweet-main')
        </div>
        <div class="flex-1 padding-20">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
    </div>

@endsection
