<a href="/logout"
    onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
    Logout
</a>

<form id="logout-form" action="/logout" method="POST" style="display: none;">
    <?php echo csrf_field() ?>
</form>
