## install git

```
sudo yum install -y git
```

###create an ssh key

```
ssh-keygen
```
and just press 'enter' to accept all the defaults

```
cat ~/.ssh/id_rsa.pub
```

and copy paste this into your BitBucket repo Access settings

```
cd [your public directory]
```

```
git clone [your repo] .
```

> don't forget the trailing '.'. That installs the repo into the current directory instead of creating a new subdirectory

## in /etc/httpd/conf/httpd.conf

```
sudo nano /etc/httpd/conf/httpd.conf
```

edit DocumentRoot to point at your public folder

change 'AllowOverride None' in /var/www/html to 'AllowOverride All'

run
```
sudo service httpd restart
```

## to install php extra extensions
```
sudo yum install -y php70 php70-mysqlnd php70-mcrypt php70-gd php70-mbstring
```

## to install composer

```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

## to install app

- go to /var/www/html (your project root)
- run `composer install`
- run `cp .env.example .env`
- run `php artisan key:generate`
- run `nano .env` -- and update database settings
- run `php artisan migrate`
- go to your site!
