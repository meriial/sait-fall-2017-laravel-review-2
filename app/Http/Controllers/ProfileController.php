<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile', [
            'user' => request()->user()
        ]);
    }

    public function postProfile()
    {
        $request = request();
        $path = $request->image->store('images', 'public');

        $user = $request->user();
        $user->profile_image = $path;
        $user->save();

        return redirect('/profile');
    }
}
